function getInfo()
	return {
		onNoUnits = RUNNING, -- instant success
		tooltip = "Load unit into atlas",
		parameterDefs = {
			{ 
				name = "atlas",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			{ 
				name = "position",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			}
		}
	}
end


function Run(self,_,parameter)
	local atlas = parameter.atlas
	local position = parameter.position
	
	if not Spring.ValidUnitID(atlas) then
		return SUCCESS
	end

	local unitIsTransporting = Spring.GetUnitIsTransporting(atlas)
	
	if (not unitIsTransporting) or #unitIsTransporting == 0 then
		return SUCCESS
	else 
		Spring.GiveOrderToUnit(atlas, CMD.UNLOAD_UNIT, {position.x,Spring.GetGroundHeight(position.x,position.z),position.z}, {})
		return RUNNING
	end
end