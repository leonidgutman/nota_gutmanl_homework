function getInfo()
	return {
		onNoUnits = RUNNING, -- instant success
		tooltip = "Load unit into atlas",
		parameterDefs = {
			{ 
				name = "atlas",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			{ 
				name = "targetUnit",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
		}
	}
end

function Run(self,_,parameter)
	local atlas = parameter.atlas
	local targetUnit = parameter.targetUnit
	
	if not Spring.ValidUnitID(atlas) then
		return SUCCESS
	end
	if not Spring.ValidUnitID(targetUnit) then
		return SUCCESS
	end
	local targetUnitTransporter = Sensors.GetUnitTransporter(targetUnit)
	
	-- validation
	if targetUnitTransporter and targetUnitTransporter ~= atlas then
		return FAILURE
	end
	
	if targetUnitTransporter == atlas then
		return SUCCESS
	end
	
	Spring.GiveOrderToUnit(atlas, CMD.LOAD_UNITS, {targetUnit}, {})
	return RUNNING
end