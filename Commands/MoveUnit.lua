function getInfo()
	return {
		onNoUnits = RUNNING,
		tooltip = "Move unit",
		parameterDefs = {
			{ 
				name = "unit",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			{ 
				name = "position",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
		}
	}
end

local DISTANCE_THRESHOLD = 20

function Run(self,_,parameter)
	local unit = parameter.unit
	local position = parameter.position
	
	if not Spring.ValidUnitID(unit) then
		return SUCCESS
	end
	
	if Sensors.GetDistanceFromUnitToPoint(unit,position) < DISTANCE_THRESHOLD then
		return SUCCESS
	else	
		Spring.GiveOrderToUnit(unit, CMD.MOVE, position:AsSpringVector(), {})
		return RUNNING
	end
end