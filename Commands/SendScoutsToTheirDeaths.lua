function getInfo()
	return {
		onNoUnits = RUNNING,
		tooltip = "Send all the scouts to the other end of the map"
	}
end

function Run(_,_,_)
	local scouts = Sensors.core.FilterUnitsByCategory(Spring.GetTeamUnits(Spring.GetMyTeamID()),Categories.nota_gutmanl_homework.scout_planes)
	if #scouts == 0 then
		return SUCCESS
	end
	for i = 1 , #scouts , 1 do
		Spring.GiveOrderToUnit(scouts[i], CMD.MOVE, {(Game.mapX/#scouts)*(i-1),0,Game.mapZ},{})
	end
	return SUCCESS
end