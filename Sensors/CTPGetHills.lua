local sensorInfo = {
	name = "CTPGetHills",
	desc = "Returns (roughly) the center of the 4 hills on the map, starting with the leftmost",
	author = "Leonid Gutman",
	date = "2019-05-05",
	license = "MIT",
}

-- get madatory module operators
-- VFS.Include("modules.lua") -- modules table
-- VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
-- attach.Module(modules, "message") -- communication backend load

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- @description return current wind statistics
return function()
	local _, maxHeight = Spring.GetGroundExtremes()
	local scanStepSize1 = 10
	local scanStepSize2 = 1
	
	function getLeftUpperPointOfHill(XOffset)
		for i = XOffset, Game.mapSizeX, scanStepSize1 do
			for j = 0, Game.mapSizeZ, scanStepSize1 do
				local height = Spring.GetGroundHeight(i, j)
				if height == maxHeight then
					return Vec3(i, 0, j)
				end
			end
		end
	end
	
	function getRightUpperPointOfHill(leftUpperPoint)
		for i = leftUpperPoint.x, Game.mapSizeX, scanStepSize2 do
			if Spring.GetGroundHeight(i, leftUpperPoint.z) < maxHeight then
				return Vec3(i - scanStepSize2, 0, leftUpperPoint.z)
			end
		end
	end
	
	function getLeftLowerPointOfHill(leftUpperPoint)
		for j = leftUpperPoint.z, Game.mapSizeZ, scanStepSize2 do
			if Spring.GetGroundHeight(leftUpperPoint.x, j) < maxHeight then
				return Vec3(leftUpperPoint.x, 0, j - scanStepSize2)
			end
		end
	end
	
	-- Modified to return a point on the left of the hill rather than the actual center.
	-- This is enough to capture the hill and the bot can get there faster.
	function getCenterOfHill(leftUpperPoint, leftLowerPoint, rightUpperPoint)
		local x = (leftUpperPoint.x*8 + rightUpperPoint.x*2)/10
		local z = (leftUpperPoint.z + leftLowerPoint.z)/2
		return Vec3(x, 0, z)
	end
	
	local leftUpperPoint1 = getLeftUpperPointOfHill(0)
	local leftLowerPoint1 = getLeftLowerPointOfHill(leftUpperPoint1)
	local rightUpperPoint1 = getRightUpperPointOfHill(leftLowerPoint1)
	local hill1 = getCenterOfHill(leftUpperPoint1, leftLowerPoint1, rightUpperPoint1)
	local leftUpperPoint2 = getLeftUpperPointOfHill(rightUpperPoint1.x + 100)
	local leftLowerPoint2 = getLeftLowerPointOfHill(leftUpperPoint2)
	local rightUpperPoint2 = getRightUpperPointOfHill(leftUpperPoint2)
	local hill2 = getCenterOfHill(leftUpperPoint2, leftLowerPoint2, rightUpperPoint2)
	local leftUpperPoint3 = getLeftUpperPointOfHill(rightUpperPoint2.x + 100)
	local leftLowerPoint3 = getLeftLowerPointOfHill(leftUpperPoint3)
	local rightUpperPoint3 = getRightUpperPointOfHill(leftUpperPoint3)
	local hill3 = getCenterOfHill(leftUpperPoint3, leftLowerPoint3, rightUpperPoint3)
	local leftUpperPoint4 = getLeftUpperPointOfHill(rightUpperPoint3.x + 100)
	local leftLowerPoint4 = getLeftLowerPointOfHill(leftUpperPoint4)
	local rightUpperPoint4 = getRightUpperPointOfHill(leftUpperPoint4)
	local hill4 = getCenterOfHill(leftUpperPoint4, leftLowerPoint4, rightUpperPoint4)
	
	return {hill1,hill2,hill3,hill4}
end