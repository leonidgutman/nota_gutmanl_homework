return function(unitID,relativeCoordinates)
	local x,y,z = Spring.GetUnitPosition(unitID)
	return Vec3(x,y,z) + relativeCoordinates
end
