local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

function getDistanceBetween2Points(point1, point2)
	return math.sqrt(math.abs(point2.x - point1.x)^2+math.abs(point2.z-point1.z)^2)
end

return function(unitID, referencePoint)
	return getDistanceBetween2Points(Vec3(Spring.GetUnitPosition(unitID)), referencePoint)
end