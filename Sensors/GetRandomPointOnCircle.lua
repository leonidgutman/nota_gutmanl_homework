local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

function generateRandomCircleVector(radius)
	local a = math.random()*2*math.pi
	local r = radius * math.sqrt(math.random())
	
	return Vec3(r*math.cos(a),0,r*math.sin(a))
end

return function(center, radius)
	local offset = generateRandomCircleVector(radius)
	return center + offset
end