local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end


return function(transportedUnit)
	local pos_x, _, pos_z = Spring.GetUnitPosition(transportedUnit)
	local radius = Spring.GetUnitRadius(transportedUnit)
	local unitTransporter = Spring.GetUnitTransporter(transportedUnit)
	
	if not Spring.ValidUnitID(transportedUnit) then
		return nil
	end
	
	local unitsInRadius = Spring.GetUnitsInCylinder(pos_x, pos_z, radius)
	local result = {}
	for i, unitID in ipairs(unitsInRadius) do
		if unitID ~= transportedUnit and unitID ~= unitTransporter then
			table.insert(result, unitID)
		end
	end
	if #result == 0 then
		return nil
	else
		return result
	end
end