local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

function getDistanceBetween2Points(point1, point2)
	return math.sqrt(math.abs(point2.x - point1.x)^2+math.abs(point2.z-point1.z)^2)
end

return function(unitIDs, referencePoint, descending)
	descending = descending or false
	table.sort(unitIDs, function(unit1, unit2)
		if not descending then
			return getDistanceBetween2Points(Vec3(Spring.GetUnitPosition(unit1)), referencePoint) < getDistanceBetween2Points(Vec3(Spring.GetUnitPosition(unit2)), referencePoint)
		else
			return getDistanceBetween2Points(Vec3(Spring.GetUnitPosition(unit1)), referencePoint) > getDistanceBetween2Points(Vec3(Spring.GetUnitPosition(unit2)), referencePoint)
		end
	end)
	return unitIDs
end